package nn;

public class Layer{

	public int neuronsNumber;
	public int inputsNumber;
	private double delta[];
	private Neuron[] neurons;
	
	public Layer(int neuronsNumber, int inputsNumber){
		
		if (neuronsNumber <= 0)
			throw new IllegalArgumentException("A layer must contain at least one neuron.");
		
		if (inputsNumber <= 0)
			throw new IllegalArgumentException("A layer must have at least one input.");
		
		this.neuronsNumber = neuronsNumber;
		this.inputsNumber = inputsNumber;
		neurons = new Neuron[neuronsNumber];
		
		for (int i = 0; i < neuronsNumber; i++)
			neurons[i] = new Neuron(inputsNumber);
		
		delta = new double[neuronsNumber];
	}
	
	public Neuron getNeuron(int n){
		
		if (n >= neurons.length || n < 0)
			throw new IllegalArgumentException("Neuron not found.");
		
		return neurons[n];
	}
	
	public void setNeuron(int k, Neuron n){
		
		if (k >= neurons.length || k < 0)
			throw new IllegalArgumentException("Wrong position trying to set a neuron.");
		
		neurons[k] = n;
	}
	
	public double getDelta(int n){
		
		if (n >= neurons.length || n < 0)
			throw new IllegalArgumentException("Neuron not found trying to get its delta.");
		
		return delta[n];
	}
	
	public void setDelta(int n, double val){
		
		if (n >= neurons.length || n < 0)
			throw new IllegalArgumentException("Neuron not found trying to set a delta.");
		
		delta[n] = val;
	}
	
	public String toString(){
		
		String s = "";
		
		s += "\tnumber of neurons: " + neuronsNumber + "\n"; 
		
		for (int i = 0; i < neuronsNumber; i++)
			s += "\t\tneuron " + (i+1) + "\n" + neurons[i].toString() + "\t\t**********\n";
		
		return s;
	}

}
