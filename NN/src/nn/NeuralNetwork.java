package nn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class NeuralNetwork {

	private Layer[] layers;
	private double eps;
	private double error;
	private double outputsExpected[];
	private double inputs[];
	
	public double outputs[];
	
	/*
	 * A network is described from the user by: 
	 * layersNumber, which states the number of layers in the network;
	 * neuronsNumber[], array that holds the number of neurons of each layer;
	 * inputsNumber[], array that holds the number of inputs of each layer's neurons.
	 * The program assumes that nth layer's neurons all have the same number of inputs.
	 * The first layer's neurons always have their weight set to one,
	 * just redirecting input to the network. Eps is the learning factor.
	 * */
	
	public NeuralNetwork(int layersNumber, int neuronsNumber[], int inputsNumber[], double eps){
		
		if (eps <= 0 || eps >= 1)
			throw new IllegalArgumentException("Learning factor must be between 0 and 1.");
		
		this.eps = eps;
		
		Random generator = new Random();
		
		outputs = new double[neuronsNumber[layersNumber-1]];
		outputsExpected = new double[neuronsNumber[layersNumber-1]];
		
		if (layersNumber < 3)
			throw new IllegalArgumentException("A network must have at least three layers.");
		
		layers = new Layer[layersNumber];
		
		for (int i = 0; i < layersNumber; i++){
			
			layers[i] = new Layer(neuronsNumber[i], inputsNumber[i]);
			
			for (int j = 0; j < layers[i].neuronsNumber; j++){
				
				Neuron n = new Neuron(inputsNumber[i]);
				
				for (int k = 0; k < inputsNumber[i]; k++){
					
					n.setWeight(k, generator.nextDouble() * 0.1);
				}
				n.bias = generator.nextDouble() * 0.1;
				
				layers[i].setNeuron(j, n);
			}
		}
		
		for (int i = 0; i < neuronsNumber[0]; i++){
			
			Neuron n = layers[0].getNeuron(i);
			n.setWeight(0, 1.0);
			n.bias = 0;
		}
	}
	
	public void evalOutputs(){
		
		//TODO check arguments in the constructor for this method to be safe!
		
		//first layer simply redirects output to other neurons
		for (int i = 0; i < layers[0].neuronsNumber; i++){
			Neuron n = layers[0].getNeuron(i);
			n.output = inputs[i];
		}
		
		for (int k = 1; k < layers.length; k++){
			
			Layer curLayer = layers[k];
			Layer preLayer = layers[k-1];
			
			for (int i = 0; i < curLayer.neuronsNumber; i++){
				
				Neuron curNeuron = curLayer.getNeuron(i);
				for(int j = 0; j < curNeuron.inputsNumber; j++){
					
					Neuron preJNeuron = preLayer.getNeuron(j);
					double outputPrec = preJNeuron.output;
					curNeuron.setInput(j, outputPrec);
				}
				curNeuron.evalActivation();
				curNeuron.evalOutput();
				if (k == layers.length -1)
					outputs[i] = curNeuron.output;
			}
		}
		
		
	}
	
	public void evalDelta(){
		
		Layer lastLayer = layers[layers.length-1];
		for (int i = 0; i < outputsExpected.length; i++){
			
			double output = lastLayer.getNeuron(i).output;
			error = outputsExpected[i] - output;
			
			double delta = error * output * (1 - output);
			lastLayer.setDelta(i, delta);
		}
		
		for (int i = layers.length-2; i > 0; i--){
			
			Layer curLayer = layers[i];
			Layer nextLayer = layers[i+1];
			
			for (int j = 0; j < curLayer.neuronsNumber; j++ ){
				
				Neuron curNeuron = curLayer.getNeuron(j);
				
				error = 0;
				for (int k = 0; k < nextLayer.neuronsNumber; k++){
					Neuron nextKNeuron = nextLayer.getNeuron(k);
					double nextDelta = nextLayer.getDelta(k);
					error += nextDelta * nextKNeuron.getWeight(j);
				}
				
				double output = curNeuron.output;
				double curDelta = error * output * (1-output);
				curLayer.setDelta(j, curDelta);
			}
		}
	}
	
	public void updateWeights(){
		
		for (int i = 1; i < layers.length; i++){
			
			Layer curLayer = layers[i];
			Layer preLayer = layers[i-1];
			for (int j = 0; j < curLayer.neuronsNumber; j++){
				
				Neuron n = curLayer.getNeuron(j);
				for (int k = 0; k < n.inputsNumber; k++){
					
					Neuron nInput = preLayer.getNeuron(k);
					double input = nInput.output;
					double weight = n.getWeight(k);
					weight += eps * curLayer.getDelta(j) * input;
					n.setWeight(k, weight);
				}
				n.bias += curLayer.getDelta(j) * eps;
			}
		}
	}
	
	public void learn(double[] input, double[] expected){
		
		this.setInput(input);
		this.outputsExpected = expected;
		
		this.evalOutputs();
		this.evalDelta();
		this.updateWeights();
	}
	
	public void learnFromSample(double[][] input, double[][] expected, int times){
		
		int inputsNumber = this.getLayer(0).inputsNumber;
		int outputsNumber = this.getLayer(layers.length-1).neuronsNumber;
		
		for (int i = 0; i < times; i++){
			
			for (int j = 0; j < input.length; j++){
				
				double[] sInput = new double[inputsNumber];
				double[] sExpected = new double[outputsNumber];
				
				for (int k = 0; k < inputsNumber; k++)
					sInput[k] = input[j][k];

				for (int k = 0; k < outputsNumber; k++)
					sExpected[k] = expected[j][k];
				
				learn(sInput, sExpected);
				
				//last iteration outputs values (maybe enable/disable full log)
				if (i == times-1){
					
					for (int k = 0; k < inputsNumber; k++)
						System.out.println("input:\t" + input[j][k]);
					
					for (int k = 0; k < outputsNumber; k++)
						System.out.println("expected:\t" + expected[j][k]);
					
					for (int k = 0; k < outputsNumber; k++)
						System.out.println("output:\t" + outputs[0]);
					
					System.out.println();
				}
			}
		}
	}
	
	public void setInput(double[] input){
		
		//TODO check first layer
		
		this.inputs = input;
	}
	
	Layer getLayer(int i){
		
		//TODO check length
		
		return layers[i];
	}
	
	public void saveNet(String fileName){
		
		try(  PrintWriter pw = new PrintWriter( fileName )  ){

			pw.println(eps);
			pw.println(layers.length);

			for (int i = 0; i < layers.length; i++){
				
				pw.println(layers[i].neuronsNumber);
			}

			for (int i = 0; i < layers.length; i++){
				
				pw.println(layers[i].inputsNumber);
			}
			
			for (int i = 0; i < layers.length; i++){
			
				Layer s = layers[i];
				for (int j = 0; j < s.neuronsNumber; j++){
					
					Neuron n = s.getNeuron(j);
					for (int k = 0; k < s.inputsNumber; k++){
						pw.println(n.getWeight(k));
					}
					
					pw.println(n.bias);
				}
			}
			
		}catch(FileNotFoundException e){
		
			System.err.println("File \"" + fileName + "\" not found.");
		}
	}
	
	public static NeuralNetwork loadNet(String fileName){
		
		File f = new File(fileName);
		
		double eps;
		int layersNumber = 0;
		int[] neuronsNumber = new int[1];
		int[] inputsNumber = new int[1];

		NeuralNetwork network = null;
		
		try ( Scanner sc = new Scanner(f) ){
			
			String line = sc.nextLine();
			
			eps = Double.parseDouble(line);
			line = sc.nextLine();
			layersNumber = Integer.parseInt(line);
			neuronsNumber = new int[layersNumber];
			inputsNumber = new int[layersNumber];
			
			for (int i = 0; i < layersNumber; i++){
				
				line = sc.nextLine();
				neuronsNumber[i] = Integer.parseInt(line);
				
			}
			
			for (int i = 0; i < layersNumber; i++){
				
				line = sc.nextLine();
				inputsNumber[i] = Integer.parseInt(line);
				
			}
			
			network = new NeuralNetwork(layersNumber, neuronsNumber, inputsNumber, eps);
			
			for (int i = 0; i < layersNumber; i++){
				
				Layer s = network.getLayer(i);
				for (int j = 0; j < s.neuronsNumber; j++){
					
					Neuron n = s.getNeuron(j);
					for (int k = 0; k < n.inputsNumber; k++){
					
						line = sc.nextLine();
						double peso = Double.parseDouble(line);
						n.setWeight(k, peso);
					}
					
					line = sc.nextLine();
					n.bias = Double.parseDouble(line);
				}
			}
			
			
		}catch(FileNotFoundException e){
			
			System.err.println("File \"" + fileName + "\" not found.");
		}
		
		System.out.println("Network loaded from: \"" + fileName + "\"");
		
		return network;
	}
	
	public String outputString(){
		
		String s = "";
		
		for (int i = 0; i < outputs.length; i++)
			s += "\noutput " + (i+1) + ": \t\t" + outputs[i];
		
		return s;
	}
	
	
	public String toString(){
		
		String s = "";
		
		s += "neural network - number of layers: " + layers.length + "\n";
		
		for (int i = 0; i < layers.length; i++)
			s += "\tlayer " + (i+1) + "\n" + layers[i].toString() + "\t**********\n";
		
		for (int i = 0; i < outputs.length; i++)
			s += "\n\noutput " + (i+1) + ": " + outputs[i];
		
		return s;
	}	
}
