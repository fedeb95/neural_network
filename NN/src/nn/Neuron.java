package nn;
public class Neuron{
	
	private double internalActivation;
	public int inputsNumber;
	private double[] inputs;
	private double[] weights;
	public double bias;
	public double output;
	
	public Neuron(int n){
	
		internalActivation = 0;
		
		if (n > 0)
			inputsNumber = n;
		else throw new IllegalArgumentException();
		
		inputs = new double[n];
		weights = new double[n];
	}
	
	public void setInput(int n, double val){
	
		if (val > 1 || val < 0)
			throw new ArithmeticException();
	
		if (n < 4 || n >= 0)
			inputs[n] = val;
		else
			throw new IllegalArgumentException();
	}
	
	public void setWeight(int n, double val){
		
		if (n < inputsNumber || n >= 0)
			weights[n] = val;
		else
			throw new IllegalArgumentException();
	}
	
	public double getWeight(int n){
		
		if (n >= inputsNumber || n < 0)
			throw new IllegalArgumentException("Peso inesistente.");
		else
			return weights[n];
	}

	public void evalActivation(){
		
		internalActivation = 0;
		
		for (int i = 0; i < inputsNumber; i++){
			
			internalActivation += (inputs[i] * weights[i]);
		}
		
		internalActivation += bias;
	}
	
	public void evalOutput(){
		
		internalActivation = -internalActivation;
		output = 1.0 / ( 1.0 + Math.exp(internalActivation) );
		if (output > 1 || output < 0)
			throw new ArithmeticException("output out of range: " + output);
	}	
	
	public String toString(){
		
		String s = "";
		
		s += "\t\tnumber of inputs: " + inputsNumber;
		
		for (int i = 0; i < weights.length; i++)
			s += "\n\t\tweight " + (i+1) + ": " + weights[i]; 
		
		s += "\n\t\tbias: " + bias;
		
		s += "\n\t\toutput: " + output + "\n";
		
		return s;
	}
}