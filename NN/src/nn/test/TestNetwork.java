package nn.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import nn.NeuralNetwork;

public class TestNetwork {
	
	public static void main(String[] args){
		
		long startMillis;
		long endMillis;
		int times = 10000;
		
		double[][] inputs = {
							{0.0},	{0.0625},	{0.1250},	{0.1875}, 
							{0.25},	{0.3125},	{0.3750},	{0.4375},
							{0.5},	{0.5625},	{0.6250},	{0.6875},
							{0.75},	{0.8125},	{0.8750},	{0.9375}, {1.0}
							}; 
		
		double[][] expected = {
							{0.5},	{0.6531},	{0.7828},	{0.8696},
							{0.9},	{0.8696},	{0.7828},	{0.6531}, 
							{0.5},	{0.3469},	{0.2172},	{0.1304},	
							{0.1},	{0.1304},	{0.2172},	{0.3469},	{0.5}
							};
		
		if (args.length > 0 && !args[0].equals("save")){
			
			NeuralNetwork network = NeuralNetwork.loadNet(args[0]);
			
			network.learnFromSample(inputs, expected, 1);
			System.out.println(network.toString());
			
		}else{
			
			int layersNumber = 3;
			int[] neuronsNumber = {1, 4, 1};
			int[] inputsNumber = {1, 1, 4};
			double eps = 0.5;
			
			NeuralNetwork network = new NeuralNetwork(layersNumber, neuronsNumber, inputsNumber, eps);
			
			startMillis = System.currentTimeMillis();
			network.learnFromSample(inputs, expected, times);
			endMillis = System.currentTimeMillis();
			
			System.out.println(network.toString());
			System.out.println("Learnin time: " + (endMillis - startMillis) + " milliseconds");
			
			if (args.length > 0){
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String nomeFile = "";
				
				try{
					
					System.out.print("File name for saving: ");
					nomeFile = br.readLine();
				
				}catch(IOException e){
				
					System.out.println("Error.");
				
				}
				
				network.saveNet(nomeFile);
			}
			
		}
		
	}
}
